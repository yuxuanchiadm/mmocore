package net.Indyuce.mmocore.guild;

public enum RelationType {

    /**
     * In the same guild
     */
    ALLY,

    /**
     * One of the two players has no guild
     */
    NEUTRAL,

    /**
     *
     */
    ENEMY;
}
